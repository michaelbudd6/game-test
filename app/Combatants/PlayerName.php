<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 17/05/2018
 * Time: 09:47
 */

declare(strict_types=1);

namespace App\Combatants;

/**
 * Class PlayerName
 * @package App\Combatants
 */
final class PlayerName
{
    /**
     * @var string
     */
    public $name;

    /**
     * PlayerName constructor.
     * @param string $playerName
     */
    private function __construct(string $playerName)
    {
        $this->name = $playerName;
    }

    /**
     * @param string $playerName
     * @return PlayerName
     */
    public static function create(string $playerName)
    {
        return new self($playerName);
    }
}