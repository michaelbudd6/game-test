<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 16/05/2018
 * Time: 09:12
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Class SetCombatantIntegerProperty
 * @package App\Combatants\Properties
 */
final class SetCombatantIntegerProperty implements CombatantPropertyInterface
{
    /**
     * @var int
     */
    private $value;

    /**
     * SetCombatantProperty constructor.
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function get(): int
    {
        return $this->value;
    }
}