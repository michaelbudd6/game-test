<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 12:05
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Class CombatantProperty
 * @package App\Combatants\Properties
 */
abstract class CombatantProperty
{
    /**
     * @var
     */
    private $value;

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return void
     */
    public function set(int $value): void
    {
        $generator      = new SetCombatantIntegerProperty($value);
        $this->value    = $generator->get();
    }

    /**
     * @param float $value
     * @return void
     */
    public function setFloat(float $value): void
    {
        $generator      = new SetCombatantFloatProperty($value);
        $this->value    = $generator->get();
    }
}