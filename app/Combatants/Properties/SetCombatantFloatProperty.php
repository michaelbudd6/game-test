<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 18/05/2018
 * Time: 21:28
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Class SetCombatantFloatProperty
 * @package App\Combatants\Properties
 */
final class SetCombatantFloatProperty implements CombatantPropertyInterface
{
    /**
     * @var float
     */
    private $value;

    /**
     * SetCombatantProperty constructor.
     * @param float $value
     */
    public function __construct(float $value)
    {
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function get(): float
    {
        return $this->value;
    }
}