<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 11:35
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Class CombatantPropertyIntegerGenerator
 * @package App\Combatants\Properties
 */
final class CombatantPropertyIntegerGenerator implements CombatantPropertyInterface
{
    /**
     * @var int
     */
    private $min;

    /**
     * @var int
     */
    private $max;

    /**
     * Speed constructor.
     * @param int $min
     * @param int $max
     */
    public function __construct(int $min, int $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * @inheritdoc
     */
    public function get()
    {
        return mt_rand($this->min, $this->max);
    }
}