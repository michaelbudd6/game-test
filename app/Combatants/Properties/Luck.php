<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 11:31
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Class Luck
 * @package App\Combatants\Properties
 */
final class Luck extends CombatantProperty
{
    /**
     * @var int
     */
    protected $value;

    /**
     * Defence constructor.
     * @param float $min
     * @param float $max
     */
    public function __construct(float $min, float $max)
    {
        $generator = new CombatantPropertyFloatGenerator($min, $max);
        parent::setFloat($generator->get());
    }
}