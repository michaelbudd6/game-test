<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 11:35
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Class CombatantPropertyIntegerGenerator
 * @package App\Combatants\Properties
 */
final class CombatantPropertyFloatGenerator implements CombatantPropertyInterface
{
    /**
     * @var float
     */
    private $min;

    /**
     * @var float
     */
    private $max;

    /**
     * Speed constructor.
     * @param float $min
     * @param float $max
     */
    public function __construct(float $min, float $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * @inheritdoc
     */
    public function get()
    {
        return mt_rand(intval($this->min * 100000), intval($this->max * 100000)) / 100000;
    }
}