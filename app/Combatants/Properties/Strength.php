<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 13/05/2018
 * Time: 20:12
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Class Speed
 * @package App\Combatants\Properties
 */
final class Strength extends CombatantProperty
{
    /**
     * Strength constructor.
     * @param int $min
     * @param int $max
     */
    public function __construct(int $min, int $max)
    {
        $generator = new CombatantPropertyIntegerGenerator($min, $max);
        parent::set($generator->get());
    }
}