<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 11:31
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Class Defence
 * @package App\Combatants\Properties
 */
final class Defence extends CombatantProperty implements CombatantPropertyInterface
{
    /**
     * @var int
     */
    protected $value;

    /**
     * Defence constructor.
     * @param int $min
     * @param int $max
     */
    public function __construct(int $min, int $max)
    {
        $generator = new CombatantPropertyIntegerGenerator($min, $max);
        parent::set($generator->get());
    }
}