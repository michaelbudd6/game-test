<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 11:45
 */

declare(strict_types=1);

namespace App\Combatants\Properties;

/**
 * Interface CombatantPropertyInterface
 * @package App\Combatants\Properties
 */
interface CombatantPropertyInterface
{
    /**
     * @return mixed
     */
    public function get();
}