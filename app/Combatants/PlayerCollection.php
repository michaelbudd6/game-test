<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 18:59
 */

declare(strict_types=1);

namespace App\Combatants;

use Illuminate\Support\Collection;

/**
 * Class PlayerCollection
 * @package App\Combatants
 */
final class PlayerCollection extends Collection
{
    /**
     * @return PlayerCollection
     */
    public function getCollectionInOrderOfNextMove(): PlayerCollection
    {
        return $this->sort(function($playerA, $playerB) {

            return $playerA->isInBetterGameStateThanPlayer($playerB);
        });
    }

    /**
     * @return PlayerCollection
     */
    public function getCollectionInOrderOfHealth(): PlayerCollection
    {
        return $this->sort(function($playerA, $playerB) {

            return $playerA->hasBetterHealthThanPlayer($playerB);
        });
    }

    /**
     * @return bool
     */
    public function onlyOnePlayerHasPositiveHealth(): bool
    {
        $playersWithPositiveHealth = $this->filter(function($player){

            return $player->health->get() > 0;
        });

        if ($playersWithPositiveHealth->count() === 1) {

            return true;
        }

        return false;
    }
}