<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 13/05/2018
 * Time: 07:38
 */

declare(strict_types=1);

namespace App\Combatants;

use App\AttackResultInterface;
use App\Combatants\Properties\Defence;
use App\Combatants\Properties\Health;
use App\Combatants\Properties\Luck;
use App\Combatants\Properties\Speed;
use App\Combatants\Properties\Strength;
use App\Combatants\SpecialStrikes\SpecialStrike;
use App\Combatants\SpecialStrikes\SpecialStrikeContainer;

/**
 * Class AbstractCombatant
 * @package App\Combatants
 */
abstract class AbstractCombatant
{
    /**
     * @var SpecialStrike
     */
    private $specialStrike;

    /**
     * @var Speed
     */
    public $speed;

    /**
     * @var Defence
     */
    public $defence;

    /**
     * @var Luck
     */
    public $luck;

    /**
     * @var Health
     */
    public $health;

    /**
     * @var Strength
     */
    public $strength;

    /**
     * @var PlayerReference
     */
    public $playerReference;

    /**
     * @var PlayerName
     */
    public $playerName;

    /**
     * AbstractCombatant constructor.
     * @param PlayerName $playerName
     * @param PlayerReference $playerReference
     * @param SpecialStrikeContainer $specialStrikeContainer
     * @param Speed $speed
     * @param Defence $defence
     * @param Health $health
     * @param Luck $luck
     * @param Strength $strength
     */
    public function __construct(
        PlayerName $playerName,
        PlayerReference $playerReference,
        SpecialStrikeContainer $specialStrikeContainer,
        Speed $speed,
        Defence $defence,
        Health $health,
        Luck $luck,
        Strength $strength
    ) {
        $this->playerName       = $playerName;
        $this->playerReference  = $playerReference;
        $this->specialStrike    = $specialStrikeContainer;
        $this->speed            = $speed;
        $this->defence          = $defence;
        $this->luck             = $luck;
        $this->health           = $health;
        $this->strength         = $strength;
    }

    /**
     * @param AbstractCombatant $defender
     * @return AttackResultInterface
     */
    public function performSpecialStrike(AbstractCombatant $defender): AttackResultInterface
    {
        if ($this->attackMisses($defender->luck)) {

            return $this->specialStrike->missed($this, $defender);
        }

        return $this->specialStrike->perform($this, $defender);
    }

    /**
     * @param Luck $playersLuck
     * @return bool
     */
    private function attackMisses(Luck $playersLuck): bool
    {
        $randomFloat = mt_rand(0, 1 * 100000) / 100000;

        if ($randomFloat <= $playersLuck->get()) {

            return true;
        }

        return false;
    }

    /**
     * @param self $anotherPlayer
     * @return int
     */
    public function isInBetterGameStateThanPlayer(AbstractCombatant $anotherPlayer)
    {
        if ($this->speed->get() === $anotherPlayer->speed->get()) {

            return ($this->defence->get() > $anotherPlayer->defence->get()) ? -1 : 1;
        }

        return ($this->speed->get() > $anotherPlayer->speed->get()) ? -1 : 1;
    }

    /**
     * @param AbstractCombatant $anotherPlayer
     * @return int
     */
    public function hasBetterHealthThanPlayer(AbstractCombatant $anotherPlayer)
    {
        if ($this->health->get() === $anotherPlayer->health->get()) {

            return 0;
        }

        return ($this->health->get() > $anotherPlayer->health->get()) ? -1 : 1;
    }

    /**
     * @param Damage $damage
     */
    public function subtractHealth(Damage $damage): void
    {
        $currentHealth  = $this->health->get();
        $newHealth      = $currentHealth - $damage->subtractFromHealth;

        if ($newHealth < 0) {

            $newHealth = 0;
        }

        $this->health->set($newHealth);
    }

    /**
     * @inheritdoc
     */
    public function __invoke(): string
    {
        return $this->playerName->name . ' has ' . $this->health->get() . ' health, and ' .
            $this->defence->get() . ' defence';
    }
}