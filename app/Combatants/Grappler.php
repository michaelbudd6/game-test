<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 13/05/2018
 * Time: 07:28
 */

declare(strict_types=1);

namespace App\Combatants;

use App\Combatants\Properties\Defence;
use App\Combatants\Properties\Health;
use App\Combatants\Properties\Luck;
use App\Combatants\Properties\Speed;
use App\Combatants\Properties\Strength;
use App\Combatants\SpecialStrikes\SpecialStrikeContainer;
use App\Combatants\SpecialStrikes\SpecialStrikeFactory;
use App\RoundEvents\RoundEventCollection;

/**
 * Class Grappler
 * @package App\Combatants
 */
final class Grappler extends AbstractCombatant
{
    private CONST MIN_SPEED = 60;
    private CONST MAX_SPEED = 80;

    private CONST MIN_DEFENCE = 35;
    private CONST MAX_DEFENCE = 40;

    private CONST MIN_HEALTH = 60;
    private CONST MAX_HEALTH = 100;

    private CONST MIN_LUCK = 0.3;
    private CONST MAX_LUCK = 0.4;

    private CONST MIN_STRENGTH = 75;
    private CONST MAX_STRENGTH = 80;

    public CONST TYPE = "Grappler";

    /**
     * Grappler constructor.
     * @param PlayerReference $playerReference
     * @param PlayerName $playerName
     */
    public function __construct(PlayerReference $playerReference, PlayerName $playerName)
    {
        parent::__construct(
            $playerName,
            $playerReference,
            new SpecialStrikeContainer(
                SpecialStrikeFactory::create(
                    Grappler::TYPE
                ),
                new RoundEventCollection()
            ),
            new Speed(self::MIN_SPEED, self::MAX_SPEED),
            new Defence(self::MIN_DEFENCE, self::MAX_DEFENCE),
            new Health(self::MIN_HEALTH, self::MAX_HEALTH),
            new Luck(self::MIN_LUCK, self::MAX_LUCK),
            new Strength(self::MIN_STRENGTH, self::MAX_STRENGTH)
        );
    }
}