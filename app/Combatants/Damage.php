<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 19/05/2018
 * Time: 21:03
 */

declare(strict_types=1);

namespace App\Combatants;

/**
 * Class Damage
 * @package App\Combatants
 */
final class Damage
{
    /**
     * @var int
     */
    public $subtractFromHealth;

    /**
     * Damage constructor.
     * @param int $subtract
     */
    public function __construct(int $subtract)
    {
        $this->subtractFromHealth = $subtract;
    }
}