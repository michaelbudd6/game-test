<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 18/05/2018
 * Time: 08:39
 */

declare(strict_types=1);

namespace App\Combatants\SpecialStrikes;

use App\AttackResult;
use App\AttackResultInterface;
use App\Combatants\AbstractCombatant;
use App\Combatants\PlayerCollection;
use App\RoundConsequences;
use App\RoundEvents\RoundEventCollection;
use Illuminate\Support\Collection;

/**
 * Class SpecialStrikeContainer
 * @package App\Combatants\SpecialStrikes
 */
final class SpecialStrikeContainer
{
    /**
     * @var SpecialStrike
     */
    private $specialStrike;

    /**
     * @var Collection
     */
    private $roundEventCollection;

    /**
     * SpecialStrikeContainer constructor.
     * @param SpecialStrike $specialStrike
     * @param Collection $roundEventCollection
     */
    public function __construct(SpecialStrike $specialStrike, Collection $roundEventCollection)
    {
        $this->specialStrike        = $specialStrike;
        $this->roundEventCollection = $roundEventCollection;
    }

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return AttackResultInterface
     */
    public function perform(AbstractCombatant $defender, AbstractCombatant $attacker): AttackResultInterface
    {
        $this->specialStrike->logHit($attacker, $defender);

        $playerCollection       = $this->specialStrike->applyPreAttackEvents($attacker, $defender);
        $attacker               = $playerCollection->get($attacker->playerReference->reference);
        $defender               = $playerCollection->get($defender->playerReference->reference);

        $playerCollection       = $this->specialStrike->applyPostAttackRules($attacker, $defender);
        $attacker               = $playerCollection->get($attacker->playerReference->reference);
        $defender               = $playerCollection->get($defender->playerReference->reference);

        $returnPlayerCollection = $this->createAndReturnPlayerCollection($attacker, $defender);
        $events                 = $this->specialStrike->getEvents();
        $roundConsequences      = $this->specialStrike->getRoundConsequences();

        $this->specialStrike->setEventCollection(new RoundEventCollection());
        $this->specialStrike->setRoundConsequences(new RoundConsequences());

        return new AttackResult($returnPlayerCollection, $roundConsequences, $events);
    }

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return PlayerCollection
     */
    private function createAndReturnPlayerCollection(
        AbstractCombatant $attacker,
        AbstractCombatant $defender
    ): PlayerCollection {

        $returnPlayerCollection = new PlayerCollection();
        $returnPlayerCollection->put($attacker->playerReference->reference, $attacker);
        $returnPlayerCollection->put($defender->playerReference->reference, $defender);

        return $returnPlayerCollection;
    }

    /**
     * @param AbstractCombatant $defender
     * @param AbstractCombatant $attacker
     * @return AttackResultInterface
     */
    public function missed(AbstractCombatant $defender, AbstractCombatant $attacker): AttackResultInterface
    {
        $returnPlayerCollection = $this->specialStrike->missed($attacker, $defender);

        return new AttackResult($returnPlayerCollection, new RoundConsequences(), $this->specialStrike->getEvents());
    }
}