<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 18/05/2018
 * Time: 22:04
 */

declare(strict_types=1);

namespace App\Combatants\SpecialStrikes;

use App\Combatants\AbstractCombatant;
use App\Combatants\Damage;
use App\Combatants\PlayerCollection;
use App\RoundConsequences\RoundConsequenceInterface;
use App\RoundEvents\EventDescription;
use App\RoundEvents\EventDescriptionInterface;
use Illuminate\Support\Collection;

abstract class SpecialStrike
{
    /**
     * @var EventDescriptionInterface
     */
    private $eventCollection;

    /**
     * @var Collection
     */
    private $roundConsequences;

    /**
     * SwordStrike constructor.
     * @param Collection $eventCollection
     * @param Collection $roundConsequences
     */
    public function __construct(Collection $eventCollection, Collection $roundConsequences)
    {
        $this->eventCollection      = $eventCollection;
        $this->roundConsequences    = $roundConsequences;
    }

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return PlayerCollection
     */
    protected function createAndReturnPlayerCollection(
        AbstractCombatant $attacker,
        AbstractCombatant $defender
    ): PlayerCollection {

        $returnPlayerCollection = new PlayerCollection();
        $returnPlayerCollection->put($attacker->playerReference->reference, $attacker);
        $returnPlayerCollection->put($defender->playerReference->reference, $defender);

        return $returnPlayerCollection;
    }

    /**
     * @param EventDescriptionInterface $eventDescription
     */
    public function addEvent(EventDescriptionInterface $eventDescription): void
    {
        $this->eventCollection->push($eventDescription);
    }

    /**
     * @param RoundConsequenceInterface $roundConsequence
     */
    public function addRoundConsequence(RoundConsequenceInterface $roundConsequence): void
    {
        $this->roundConsequences->push($roundConsequence);
    }

    /**
     * @return Collection
     */
    public function getEvents(): Collection
    {
        return $this->eventCollection;
    }

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return PlayerCollection
     */
    public function missed(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        $eventDescription = $attacker->playerName->name . '\'s attack missed!';
        $this->addEvent(new EventDescription($eventDescription));

        return $this->createAndReturnPlayerCollection($attacker, $defender);
    }

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return PlayerCollection
     */
    public function applyPostAttackRules(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        $returnPlayerCollection = $this->applyDamage($attacker, $defender);
        return $returnPlayerCollection;
    }

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return PlayerCollection
     */
    private function applyDamage(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        $damage             = $attacker->strength->get() - $defender->defence->get();
        $eventDescription   = $attacker->playerName->name . ' knocked ' . $damage .
            ' off ' . $defender->playerName->name . '\'s health';
        $this->addEvent(new EventDescription($eventDescription));
        $defender->subtractHealth(new Damage($damage));

        $returnPlayerCollection = $this->createAndReturnPlayerCollection($attacker, $defender);
        return $returnPlayerCollection;
    }

    /**
     * @param Collection $collection
     * @return void
     */
    public function setEventCollection(Collection $collection): void
    {
        $this->eventCollection = $collection;
    }

    /**
     * @param Collection $collection
     */
    public function setRoundConsequences(Collection $collection): void
    {
        $this->roundConsequences = $collection;
    }

    /**
     * @return Collection
     */
    public function getRoundConsequences(): Collection
    {
        return $this->roundConsequences;
    }
}