<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 13/05/2018
 * Time: 07:39
 */

declare(strict_types=1);

namespace App\Combatants\SpecialStrikes;

use App\Combatants\AbstractCombatant;
use App\Combatants\PlayerCollection;
use App\RoundEvents\EventDescriptionInterface;
use Illuminate\Support\Collection;

/**
 * Interface SpecialStrike
 * @package App\Combatants\SpecialStrikes
 */
interface SpecialStrikeInterface
{
    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return PlayerCollection
     */
    public function applyPostAttackRules(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection;

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return PlayerCollection
     */
    public function applyPreAttackEvents(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection;

    /**
     * @return Collection
     */
    public function getEvents(): Collection;

    /**
     * @param EventDescriptionInterface $eventDescription
     */
    public function addEvent(EventDescriptionInterface $eventDescription): void;

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return PlayerCollection
     */
    public function missed(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection;

    /**
     * @param AbstractCombatant $attacker
     * @param AbstractCombatant $defender
     * @return void
     */
    public function logHit(AbstractCombatant $attacker, AbstractCombatant $defender): void;
}