<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 12:28
 */

declare(strict_types=1);

namespace App\Combatants\SpecialStrikes;

use App\Combatants\AbstractCombatant;
use App\Combatants\Damage;
use App\Combatants\PlayerCollection;
use App\RoundEvents\EventDescription;
use Illuminate\Support\Collection;

/**
 * Class Grapple
 * @package App\Combatants\SpecialStrikes
 */
final class Grapple extends SpecialStrike implements SpecialStrikeInterface
{
    public CONST TYPE = 'Grapple';

    /**
     * Grapple constructor.
     * @param Collection $eventCollection
     * @param Collection $roundConsequences
     */
    public function __construct(Collection $eventCollection, Collection $roundConsequences)
    {
        parent::__construct($eventCollection, $roundConsequences);
    }

    /**
     * @inheritdoc
     */
    public function applyPreAttackEvents(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        $returnPlayerCollection = $this->createAndReturnPlayerCollection($attacker, $defender);

        return $returnPlayerCollection;
    }

    /**
     * @inheritdoc
     */
    public function logHit(AbstractCombatant $attacker, AbstractCombatant $defender): void
    {
        $hitEvent = $attacker->playerName->name . ' dealt their special strike, the ' .
            'Grapple on ' . $defender->playerName->name;

        $this->addEvent(new EventDescription($hitEvent));
    }

    /**
     * @inheritdoc
     */
    public function missed(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        $eventDescription   = $defender->playerName->name . ' evaded an attack from ' .
            $attacker->playerName->name . ', and knocked off 10 from their health';
        $this->addEvent(new EventDescription($eventDescription));
        $attacker->subtractHealth(new Damage(10));

        return $this->createAndReturnPlayerCollection($attacker, $defender);
    }
}