<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 12:28
 */

declare(strict_types=1);

namespace App\Combatants\SpecialStrikes;

use App\Combatants\AbstractCombatant;
use App\Combatants\PlayerCollection;
use App\RoundEvents\EventDescription;
use Illuminate\Support\Collection;

/**
 * Class SwordStrike
 * @package App\Combatants\SpecialStrikes
 */
final class SwordStrike extends SpecialStrike implements SpecialStrikeInterface
{
    public CONST TYPE = 'Sword Strike';

    /**
     * SwordStrike constructor.
     * @param Collection $eventCollection
     * @param Collection $roundConsequences
     */
    public function __construct(Collection $eventCollection, Collection $roundConsequences)
    {
        parent::__construct($eventCollection, $roundConsequences);
    }

    /**
     * @inheritdoc
     */
    public function applyPreAttackEvents(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        if (mt_rand(1, 100) <= 5) {

            $eventDescription = $attacker->playerName->name . '\'s strength was doubled';
            $this->addEvent(new EventDescription($eventDescription));
            $newStrength = $attacker->strength->get() * 2;
            $attacker->strength->set($newStrength);
        }

        $returnPlayerCollection = $this->createAndReturnPlayerCollection($attacker, $defender);

        return $returnPlayerCollection;
    }

    /**
     * @inheritdoc
     */
    public function logHit(AbstractCombatant $attacker, AbstractCombatant $defender): void
    {
        $hitEvent = $attacker->playerName->name . ' dealt their special strike, the ' .
                    'Sword Strike on ' . $defender->playerName->name;

        $this->addEvent(new EventDescription($hitEvent));
    }

    /**
     * @inheritdoc
     */
    public function missed(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        return $this->createAndReturnPlayerCollection($attacker, $defender);
    }
}
