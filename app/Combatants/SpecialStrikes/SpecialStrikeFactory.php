<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 18/05/2018
 * Time: 20:12
 */

declare(strict_types=1);

namespace App\Combatants\SpecialStrikes;

use App\Combatants\Brute;
use App\Combatants\Grappler;
use App\Combatants\Swordsman;
use App\RoundConsequences;
use App\RoundEvents\RoundEventCollection;

/**
 * Class SpecialStrikeFactory
 * @package App\Combatants\SpecialStrikes
 */
final class SpecialStrikeFactory
{
    /**
     * @param string $combatantType
     * @return SpecialStrike
     */
    public static function create(string $combatantType): SpecialStrike
    {
        switch($combatantType) {

            case Swordsman::TYPE:

                return new SwordStrike(new RoundEventCollection(), new RoundConsequences());

                break;

            case Brute::TYPE:

                return new StunningBlow(new RoundEventCollection(), new RoundConsequences());

                break;

            case Grappler::TYPE:

                return new Grapple(new RoundEventCollection(), new RoundConsequences());

                break;
        }
    }
}