<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 12:28
 */

declare(strict_types=1);

namespace App\Combatants\SpecialStrikes;

use App\Combatants\AbstractCombatant;
use App\Combatants\PlayerCollection;
use App\RoundConsequences\MissNextRound;
use App\RoundEvents\EventDescription;
use Illuminate\Support\Collection;

/**
 * Class Grapple
 * @package App\Combatants\SpecialStrikes
 */
final class StunningBlow extends SpecialStrike implements SpecialStrikeInterface
{
    public CONST TYPE = 'Stunning Blow';

    /**
     * Grapple constructor.
     * @param Collection $eventCollection
     * @param Collection $roundConsequences
     */
    public function __construct(Collection $eventCollection, Collection $roundConsequences)
    {
        parent::__construct($eventCollection, $roundConsequences);
    }

    /**
     * @inheritdoc
     */
    public function applyPreAttackEvents(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        $returnPlayerCollection = $this->createAndReturnPlayerCollection($attacker, $defender);

        return $returnPlayerCollection;
    }

    /**
     * @inheritdoc
     */
    public function logHit(AbstractCombatant $attacker, AbstractCombatant $defender): void
    {
        $hitEvent = $attacker->playerName->name . ' dealt their special strike, the ' .
            self::TYPE . ' on ' . $defender->playerName->name;

        $this->addEvent(new EventDescription($hitEvent));

        if (mt_rand(1, 100) <= 100) {

            $eventDescription = 'Following a ' . self::TYPE . ' ' . $defender->playerName->name .
                ' will miss their next go!';

            $event = new EventDescription($eventDescription);
            $this->addEvent($event);

            $roundConsequence = new MissNextRound($defender);
            $this->addRoundConsequence($roundConsequence);
        }
    }

    /**
     * @inheritdoc
     */
    public function missed(AbstractCombatant $attacker, AbstractCombatant $defender): PlayerCollection
    {
        return $this->createAndReturnPlayerCollection($attacker, $defender);
    }
}
