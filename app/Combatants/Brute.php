<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 12:23
 */

declare(strict_types=1);

namespace App\Combatants;

use App\Combatants\Properties\Defence;
use App\Combatants\Properties\Health;
use App\Combatants\Properties\Luck;
use App\Combatants\Properties\Speed;
use App\Combatants\Properties\Strength;
use App\Combatants\SpecialStrikes\SpecialStrikeContainer;
use App\Combatants\SpecialStrikes\SpecialStrikeFactory;
use App\RoundEvents\RoundEventCollection;

/**
 * Class Swordsman
 * @package App\Combatants
 */
final class Brute extends AbstractCombatant
{
    private CONST MIN_SPEED = 40;
    private CONST MAX_SPEED = 65;

    private CONST MIN_DEFENCE = 40;
    private CONST MAX_DEFENCE = 50;

    private CONST MIN_HEALTH = 90;
    private CONST MAX_HEALTH = 100;

    private CONST MIN_LUCK = 0.3;
    private CONST MAX_LUCK = 0.35;

    private CONST MIN_STRENGTH = 65;
    private CONST MAX_STRENGTH = 70;

    public CONST TYPE = "Brute";

    /**
     * Swordsman constructor.
     * @param PlayerReference $playerReference
     * @param PlayerName $playerName
     */
    public function __construct(PlayerReference $playerReference, PlayerName $playerName)
    {
        parent::__construct(
            $playerName,
            $playerReference,
            new SpecialStrikeContainer(
                SpecialStrikeFactory::create(Brute::TYPE),
                new RoundEventCollection()
            ),
            new Speed(self::MIN_SPEED, self::MAX_SPEED),
            new Defence(self::MIN_DEFENCE, self::MAX_DEFENCE),
            new Health(self::MIN_HEALTH, self::MAX_HEALTH),
            new Luck(self::MIN_LUCK, self::MAX_LUCK),
            new Strength(self::MIN_STRENGTH, self::MAX_STRENGTH)
        );
    }
}