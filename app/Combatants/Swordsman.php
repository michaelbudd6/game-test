<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 12:23
 */

namespace App\Combatants;

use App\Combatants\Properties\Defence;
use App\Combatants\Properties\Health;
use App\Combatants\Properties\Luck;
use App\Combatants\Properties\Speed;
use App\Combatants\Properties\Strength;
use App\Combatants\SpecialStrikes\SpecialStrikeContainer;
use App\Combatants\SpecialStrikes\SpecialStrikeFactory;
use App\Combatants\SpecialStrikes\SwordStrike;
use App\RoundEvents\RoundEventCollection;

/**
 * Class Swordsman
 * @package App\Combatants
 */
final class Swordsman extends AbstractCombatant
{
    private CONST MIN_SPEED = 90;
    private CONST MAX_SPEED = 100;

    private CONST MIN_DEFENCE = 20;
    private CONST MAX_DEFENCE = 30;

    private CONST MIN_HEALTH = 40;
    private CONST MAX_HEALTH = 60;

    private CONST MIN_LUCK = 0.3;
    private CONST MAX_LUCK = 0.5;

    private CONST MIN_STRENGTH = 60;
    private CONST MAX_STRENGTH = 70;

    public CONST TYPE = "Swordsman";

    /**
     * Swordsman constructor.
     * @param PlayerReference $playerReference
     * @param PlayerName $playerName
     */
    public function __construct(PlayerReference $playerReference, PlayerName $playerName)
    {
        parent::__construct(
            $playerName,
            $playerReference,
            new SpecialStrikeContainer(
                SpecialStrikeFactory::create(Swordsman::TYPE),
                new RoundEventCollection()
            ),
            new Speed(self::MIN_SPEED, self::MAX_SPEED),
            new Defence(self::MIN_DEFENCE, self::MAX_DEFENCE),
            new Health(self::MIN_HEALTH, self::MAX_HEALTH),
            new Luck(self::MIN_LUCK, self::MAX_LUCK),
            new Strength(self::MIN_STRENGTH, self::MAX_STRENGTH)
        );
    }
}