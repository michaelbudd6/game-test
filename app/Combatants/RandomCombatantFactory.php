<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 15/05/2018
 * Time: 20:32
 */

declare(strict_types=1);

namespace App\Combatants;

/**
 * Class RandomCombatantFactory
 * @package App\Combatants
 */
final class RandomCombatantFactory
{
    /**
     * @param int $playerNumber
     * @param string $combatantType
     * @param string $playerName
     * @return AbstractCombatant
     */
    public static function generate(int $playerNumber, string $combatantType, string $playerName): AbstractCombatant
    {
        $playerRef  = PlayerReference::create($playerNumber, $playerName);
        $playerName = PlayerName::create($playerName);

        switch ($combatantType) {

            case "Swordsman":

                return new Swordsman($playerRef, $playerName);

                break;

            case "Brute":

                return new Brute($playerRef, $playerName);

                break;

            case "Grappler":

                return new Grappler($playerRef, $playerName);

                break;
        }
    }
}