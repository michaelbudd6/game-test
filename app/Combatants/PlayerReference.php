<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 16/05/2018
 * Time: 21:07
 */

declare(strict_types=1);

namespace App\Combatants;

/**
 * Class PlayerReference
 * @package App\Combatants
 */
final class PlayerReference
{
    /**
     * @var string
     */
    public $reference;

    /**
     * PlayerReference constructor.
     * @param int $playerNumber
     * @param string $playerName
     */
    private function __construct(int $playerNumber, string $playerName)
    {
        $this->reference = $playerNumber . $playerName;
    }

    /**
     * @param int $playerNumber
     * @param string $playerName
     * @return PlayerReference
     */
    public static function create(int $playerNumber, string $playerName)
    {
        return new self($playerNumber, $playerName);
    }
}