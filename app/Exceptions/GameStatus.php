<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 21/05/2018
 * Time: 07:50
 */

namespace App\Exceptions;

use Exception;

/**
 * Class GameHasEnded
 * @package App\Exceptions
 */
final class GameStatus extends Exception
{
    /**
     * @return GameStatus
     */
    public static function hasAlreadyEnded(): GameStatus
    {
        return new self('The game has ended');
    }
}