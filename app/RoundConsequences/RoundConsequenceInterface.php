<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 20/05/2018
 * Time: 19:52
 */

declare(strict_types=1);

namespace App\RoundConsequences;

use App\Combatants\AbstractCombatant;

interface RoundConsequenceInterface
{
    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @param AbstractCombatant $player
     * @return bool
     */
    public function isSamePlayerAs(AbstractCombatant $player): bool;
}