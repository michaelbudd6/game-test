<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 20/05/2018
 * Time: 19:54
 */

declare(strict_types=1);

namespace App\RoundConsequences;

use App\Combatants\AbstractCombatant;

final class MissNextRound implements RoundConsequenceInterface
{
    private CONST TYPE = "MissNextRound";

    /**
     * @var AbstractCombatant
     */
    private $playerToMissAGo;

    /**
     * MissNextRound constructor.
     * @param AbstractCombatant $player
     */
    public function __construct(AbstractCombatant $player)
    {
        $this->playerToMissAGo = $player;
    }

    /**
     * @inheritdoc
     */
    public function isSamePlayerAs(AbstractCombatant $player): bool
    {
        if ($player->playerReference->reference === $this->playerToMissAGo->playerReference->reference) {

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function getType(): string
    {
        return self::TYPE;
    }
}