<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 15/05/2018
 * Time: 07:57
 */

declare(strict_types=1);

namespace App;

/**
 * Class Console
 * @package App
 */
final class Console implements ConsoleInterface
{
    /**
     * @inheritdoc
     */
    public function out(string $msg): void
    {
        echo $msg . "\r\n";
    }

    /**
     * @inheritdoc
     */
    public function in(): ? string
    {
        return trim(fgets(STDIN));
    }
}