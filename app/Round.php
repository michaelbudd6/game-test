<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 14/05/2018
 * Time: 19:11
 */

declare(strict_types=1);

namespace App;

use App\Combatants\AbstractCombatant;
use App\Combatants\PlayerCollection;
use App\Exceptions\GameStatus;
use Illuminate\Support\Collection;

/**
 * Class Round
 * @package App
 */
final class Round
{
    /**
     * @var PlayerCollection
     */
    private $playerCollection;


    /**
     * @var int
     */
    public $roundNumber;

    /**
     * @var Console
     */
    private $console;

    /**
     * @var Collection
     */
    private $roundEvents;

    /**
     * @var Collection
     */
    private $roundConsequences;

    /**
     * Round constructor.
     * @param ConsoleInterface $console
     * @param int $roundNumber
     * @param Collection $playerCollection
     * @param Collection $nextRoundConsequences
     */
    public function __construct(
        ConsoleInterface $console,
        int $roundNumber,
        Collection $playerCollection,
        Collection $nextRoundConsequences
    ) {
        $this->console              = $console;
        $this->roundNumber          = $roundNumber;
        $this->playerCollection     = $playerCollection;
        $this->roundEvents          = new Collection();
        $this->roundConsequences    = $nextRoundConsequences;
    }

    /**
     * @return EndOfRound
     */
    public function run(): EndOfRound
    {
        $this->printRoundStartMessage();
        $thisRoundConsequences = $this->roundConsequences;

        $this->playerCollection->each(function ($player) use ($thisRoundConsequences) {

            $missAGo = $this->shouldMissAGo($player, $thisRoundConsequences);

            if (FALSE === $missAGo) {

                try {

                    $this->commenceCombat($player);

                } catch (GameStatus $exception) {

                    $this->console->out($exception->getMessage());
                }
            }
        });

        $this->console->out("\r\n-------------------\r\n");

        return new EndOfRound($this->playerCollection, $this->roundConsequences);
    }

    private function commenceCombat(AbstractCombatant $attacker): void {

        $this->playerCollection->each(function ($playerToDefend) use ($attacker) {

            if ($this->playerCollection->onlyOnePlayerHasPositiveHealth()) {

                throw GameStatus::hasAlreadyEnded();

            } else {
                if ($attacker->playerReference->reference !== $playerToDefend->playerReference->reference) {

                    $attackResult               = $attacker->performSpecialStrike($playerToDefend);
                    $this->roundConsequences    = $attackResult->getNextRoundConsequences();
                    $roundEvents                = $attackResult->getRoundEvents();

                    $this->updatePlayerCollection($attackResult->getPlayerCollection());
                    $roundEvents->print($this->console);
                }
            }
        });
    }

    /**
     * @param AbstractCombatant $player
     * @param Collection $roundConsequences
     * @return bool
     */
    private function shouldMissAGo(AbstractCombatant $player, Collection $roundConsequences): bool
    {
        $missAGo = false;

        foreach ($roundConsequences as $consequence) {
            if (TRUE === $consequence->isSamePlayerAs($player)) {

                $missAGo = true;
                $this->console->out($player->playerName->name . ' is missing this go');

                break;
            }
        }

        return $missAGo;
    }

    private function printRoundStartMessage(): void
    {
        $roundStartDescription = 'Round ' . $this->roundNumber . ' ...';
        $this->console->out($roundStartDescription . "\r\n");
        $this->printPlayerHealth();
    }

    private function printPlayerHealth(): void
    {
        $this->playerCollection->each(function ($player) {

            $playerHealthDescription = $player->playerName->name . ' has ' .
                $player->health->get() . ' health at the beginning of round ' .
                $this->roundNumber;

            $this->console->out($playerHealthDescription);
        });
    }

    /**
     * @param PlayerCollection $players
     */
    private function updatePlayerCollection(PlayerCollection $players): void
    {
        $players->each(function ($player) {

            $this->playerCollection->put($player->playerReference->reference, $player);
        });
    }
}