<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 15/05/2018
 * Time: 07:58
 */

declare(strict_types=1);

namespace App;

/**
 * Interface LoggerInterface
 * @package App
 */
interface ConsoleInterface
{
    /**
     * @param string $msg
     */
    public function out(string $msg): void;

    /**
     * @return null|string
     */
    public function in(): ? string;
}