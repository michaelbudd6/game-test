<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 18/05/2018
 * Time: 19:14
 */

namespace App\RoundEvents;

/**
 * Class EventDescription
 * @package App\RoundEvents
 */
class EventDescription implements EventDescriptionInterface
{
    /**
     * @var string
     */
    private $eventDescription;

    /**
     * EventDescription constructor.
     * @param string $eventDescription
     */
    public function __construct(string $eventDescription)
    {
        $this->eventDescription = $eventDescription;
    }

    /**
     * @return string
     */
    public function get(): string
    {
        return $this->eventDescription;
    }
}