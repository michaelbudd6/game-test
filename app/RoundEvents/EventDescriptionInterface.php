<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 18/05/2018
 * Time: 19:14
 */

declare(strict_types=1);

namespace App\RoundEvents;

/**
 * Interface EventDescriptionInterface
 * @package App\RoundEvents
 */
interface EventDescriptionInterface
{
    /**
     * @return string
     */
    public function get(): string;
}