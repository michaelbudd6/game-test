<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 18/05/2018
 * Time: 12:46
 */

declare(strict_types=1);

namespace App\RoundEvents;

use App\ConsoleInterface;

/**
 * Class RoundEventCollection
 */
final class RoundEventCollection extends \Illuminate\Support\Collection
{
    /**
     * @inheritdoc
     */
    public function print(ConsoleInterface $console): void
    {
        $this->each(function($event) use ($console){

            $console->out($event->get());
        });
    }
}