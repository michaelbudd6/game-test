<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 17/05/2018
 * Time: 22:18
 */

declare(strict_types=1);

namespace App;

use App\Combatants\PlayerCollection;
use Illuminate\Support\Collection;

/**
 * Class AttackResult
 * @package App
 */
final class AttackResult implements AttackResultInterface
{
    /**
     * @var PlayerCollection
     */
    private $playerCollection;

    /**
     * @var RoundConsequences
     */
    private $roundConsequences;

    /**
     * @var Collection
     */
    private $roundEvents;

    /**
     * AttackResult constructor.
     * @param PlayerCollection $playerCollection
     * @param RoundConsequences $roundConsequences
     * @param Collection $roundEvents
     */
    public function __construct(
        PlayerCollection $playerCollection,
        RoundConsequences $roundConsequences,
        Collection $roundEvents
    ) {
        $this->playerCollection     = $playerCollection;
        $this->roundConsequences    = $roundConsequences;
        $this->roundEvents          = $roundEvents;
    }

    /**
     * @inheritdoc
     */
    public function getPlayerCollection(): PlayerCollection
    {
        return $this->playerCollection;
    }

    /**
     * @inheritdoc
     */
    public function getNextRoundConsequences(): RoundConsequences
    {
        return $this->roundConsequences;
    }

    /**
     * @inheritdoc
     */
    public function getRoundEvents(): Collection
    {
        return $this->roundEvents;
    }
}