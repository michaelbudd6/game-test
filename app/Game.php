<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 13/05/2018
 * Time: 07:45
 */

namespace App;

use App\Combatants\Brute;
use App\Combatants\Grappler;
use App\Combatants\PlayerCollection;
use App\Combatants\RandomCombatantFactory;
use App\Combatants\Swordsman;
use Illuminate\Support\Collection;

/**
 * Class Game
 * @package App
 */
final class Game
{
    private CONST NUMBER_OF_PLAYERS = 2;

    private CONST NUMBER_OF_ROUNDS = 5;

    /**
     * @var PlayerCollection
     */
    private $players;

    /**
     * @var Rounds
     */
    private $rounds;

    /**
     * @var ConsoleInterface
     */
    private $console;

    /**
     * @var Collection
     */
    private $nextRoundConsequences;

    /**
     * Game constructor.
     * @param ConsoleInterface $console
     */
    public function __construct(ConsoleInterface $console)
    {
        $this->console = $console;
        $this->players = new PlayerCollection();
        $this->nextRoundConsequences = new Collection();
        $this->rounds = new Rounds();

        $this->createPlayers();
        $this->console->out('Game Started!');
        $this->run();
    }

    private function createPlayers(): void
    {
        $players = new PlayerCollection();

        for ($i = 1; $i <= self::NUMBER_OF_PLAYERS; $i++) {
            $this->console->out('Please enter player ' . $i . ' name:');
            $playerName = $this->console->in();
            $combatantTypes = [Grappler::TYPE, Brute::TYPE, Swordsman::TYPE];
            $player = RandomCombatantFactory::generate($i, $combatantTypes[mt_rand(0, 2)], $playerName);

            $players->put($player->playerReference->reference, $player);

            $this->console->out('Player added to the game: ' . $player->playerName->name . "\r\n");
            $this->console->out($player() . "\r\n");
            sleep(1);
        }

        $this->players = $players;
    }

    private function run(): void
    {
        for ($i = 1; $i <= self::NUMBER_OF_ROUNDS; $i++) {
            $playersInRoundMoveOrder = $this->players->getCollectionInOrderOfNextMove();
            $round = new Round($this->console, $i, $playersInRoundMoveOrder, $this->nextRoundConsequences);
            $endOfRound = $round->run();

            $this->rounds->push($round);
            $this->players = $endOfRound->players;
            $this->nextRoundConsequences = $endOfRound->nextRoundConsequences;

            if ($this->players->onlyOnePlayerHasPositiveHealth() ||
                $i === self::NUMBER_OF_ROUNDS) {

                $this->displayEndOfGameMessage($round->roundNumber);

                break;
            }

            sleep(1);
        }
    }

    /**
     * @param int $roundNumber
     * @return void
     */
    private function displayEndOfGameMessage(int $roundNumber): void
    {
        $playersInOrderOfHealthDESC = $this->players->getCollectionInOrderOfHealth();
        $winner = $playersInOrderOfHealthDESC->first();

        $this->console->out($winner->playerName->name . ' wins the game in round ' . $roundNumber . '!');

        $playersInOrderOfHealthDESC->each(function ($player) {

            $this->console->out($player());
        });
    }
}