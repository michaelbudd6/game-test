<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 17/05/2018
 * Time: 22:18
 */

declare(strict_types=1);

namespace App;

use App\Combatants\PlayerCollection;
use Illuminate\Support\Collection;

/**
 * Interface AttackResultInterface
 * @package App
 */
interface AttackResultInterface
{
    /**
     * @return PlayerCollection
     */
    public function getPlayerCollection(): PlayerCollection;

    /**
     * @return Collection
     */
    public function getRoundEvents(): Collection;

    /**
     * @return RoundConsequences
     */
    public function getNextRoundConsequences(): RoundConsequences;
}