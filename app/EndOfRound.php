<?php
/**
 * Created by PhpStorm.
 * User: michaelbudd
 * Date: 18/05/2018
 * Time: 12:59
 */

declare(strict_types=1);

namespace App;

use App\Combatants\PlayerCollection;
use Illuminate\Support\Collection;

/**
 * Class EndOfRound
 * @package App
 */
final class EndOfRound
{
    /**
     * @var PlayerCollection
     */
    public $players;

    /**
     * @var Collection
     */
    public $nextRoundConsequences;

    /**
     * EndOfRound constructor.
     * @param Collection $players
     * @param Collection $nextRoundConsequences
     */
    public function __construct(Collection $players, Collection $nextRoundConsequences)
    {
        $this->players                  = $players;
        $this->nextRoundConsequences    = $nextRoundConsequences;
    }
}